const HDWalletProvider = require('@truffle/hdwallet-provider');

const fs = require('fs');
const secret = fs.readFileSync(".secret");

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*",
      gas: 6721975,
      gasLimit: 6721975,
      gasPrice: 1,
    },

    ropsten: {
      network_id: "3",
      provider: () =>
          new HDWalletProvider(
              secret["mnemonic"],
              "https://ropsten.infura.io/v3/"+secret["infura"]
          ),
      gasPrice: 10000000000, // 10 gwei
      gas: 6900000,
      //from: account, 
    },
    kovan: {
      network_id: "42",
      provider: () =>
          new HDWalletProvider(
              secret["mnemonic"],
              "https://kovan.infura.io/v3/"+secret["infura"]
          ),
      gasPrice: 10000000000, // 10 gwei
      gas: 6900000,
      //from: account,
      timeoutBlocks: 500,
    },
  },

  compilers: {
    solc: {
      version: '0.8.6',
        docker: false,
        settings: {
          optimizer: {
            enabled: false,
            runs: 10000,
          },
        },
    }
  },
  plugins: [
    "truffle-plugin-verify",
    'truffle-plugin-solhint'
  ],
  api_keys: {
    etherscan: secret["etherscan"]
  }
};
