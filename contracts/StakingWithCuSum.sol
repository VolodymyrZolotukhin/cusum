// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.6;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract StakingWithCuSum {

    uint256 constant public DECIMAL = 10**18;

    IERC20 public token;
    
    mapping(address => UserInfo) public userStakes;

    uint256 public lastBlock;
    uint256 public rewardPerBlock;
    uint256 public sum;
    uint256 public totalStaked;


    struct UserInfo{
        uint256 staked;
        uint256 lastSum;
        uint256 reward;
    }

    constructor (IERC20 _token, uint256 _rewardPerBlock) {
        token = _token;
        rewardPerBlock = _rewardPerBlock;
    }

    function stake(uint256 _amount) external {
        _update();
        userStakes[msg.sender].staked += _amount;
        totalStaked += _amount;
        token.transferFrom(msg.sender, address(this), _amount);
    }

    function withdraw(uint256 _amount) external {
        _update();
        require(userStakes[msg.sender].staked >= _amount, "Amount more then staked");
        userStakes[msg.sender].staked -= _amount;
        totalStaked -= _amount;
        token.transfer(msg.sender, _amount);
    }

    function drawReward(uint256 _amount) external {
        _update();
        require(userStakes[msg.sender].reward >= _amount, "Amount more then reward");
        userStakes[msg.sender].reward = 0;
        token.transfer(msg.sender, _amount);
    }

    function updateRewardPerBlock(uint256 _newReward) external {
        _updateSum();
        rewardPerBlock = _newReward;
    }

    function _update() internal {
        _updateSum();
        _updateReward();
    }

    function _updateSum() internal {
        uint256 _last = lastBlock == 0 ? block.number : lastBlock;
        uint256 _total = totalStaked;
        
        if (_total > 0){
            sum += (block.number - _last) * (rewardPerBlock * DECIMAL / _total);
        }

        lastBlock = block.number;
    }

    function _updateReward() internal {
        uint256 _userStake = userStakes[msg.sender].staked;
        
        if (_userStake > 0){
            userStakes[msg.sender].reward += ((sum - userStakes[msg.sender].lastSum) * _userStake) / DECIMAL;
        }

        userStakes[msg.sender].lastSum = sum;
    }
}