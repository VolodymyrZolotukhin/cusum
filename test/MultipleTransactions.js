const Staking = artifacts.require("StakingWithCuSum");
const TestCoin = artifacts.require("./mock/ERC20Mock.sol");
const TimeTravaler = require("ganache-time-traveler");
const Reverter = require("./helpers/reverter");

contract("Stake", async(accounts) => {
    const OWNER = accounts[0];
    
    const reverter = new Reverter(web3);
    const AMOUNT = "100";
    const rewardPerBlock = "10"
    

    let testCoin;
    let staker;
    let snapshot;
    
    async function revert(){
        reverter.revert()
        TimeTravaler.revertToSnapshot(snapshot);
    }
    
    before("Setup", async() => {
        testCoin = await TestCoin.new();
        staker = await Staking.new(testCoin.address, rewardPerBlock);
        
        snapshot = TimeTravaler.takeSnapshot();
        reverter.snapshot()    
    });

    afterEach("revert", revert);

    describe("Multiple transactions in one block", async() => {
        it("should create BatchRequest with two tx", async() => {
            await testCoin.approve(staker.address, AMOUNT*2+1);
            const batch = new web3.BatchRequest();
            const Contract = new web3.eth.Contract(staker.abi, staker.address);
            
            let block = (await web3.eth.getBlock("latest"))["number"];
            
            batch.add(Contract.methods.stake(AMOUNT).send.request({from:OWNER, gas:1000000}));
            batch.add(Contract.methods.stake(AMOUNT).send.request({from:OWNER, gas:1000000}));
            batch.execute();

            assert.equal((await web3.eth.getBlock("latest"))["number"], block);
            assert.equal((await staker.userStakes(OWNER)).reward.toString(), 0);
            await staker.stake(1);
            assert.equal((await web3.eth.getBlock("latest"))["number"], block+2);
            assert.equal((await staker.userStakes(OWNER)).reward.toString(), 10);
        });
    });
});
