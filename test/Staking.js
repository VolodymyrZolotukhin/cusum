const Staking = artifacts.require("StakingWithCuSum");
const TestCoin = artifacts.require("./mock/ERC20Mock.sol");
const TimeTravaler = require("ganache-time-traveler");
const Reverter = require("./helpers/reverter");
const truffleAssert = require("truffle-assertions");


contract("Stake", async(accounts) => {
    const OWNER = accounts[0];
    const ALICE = accounts[1];
    const RON = accounts[2];
    const BOB = accounts[3];
    const USER4 = accounts[4];
    const reverter = new Reverter(web3);
    const AMOUNT = "100";
    const rewardPerBlock = "10"
    const decimal = toBN("10").pow(toBN("18"));
    const blocks = 9;

    let testCoin;
    let staker;
    let snapshot;
    let balance;
    
    function revert(){
        reverter.revert()
        TimeTravaler.revertToSnapshot(snapshot);
    }

    function toBN(str){
        return web3.utils.toBN(str);
    }

    async function advanceBlocks(num) {
        for (i = 0;i<num;i++)
            await TimeTravaler.advanceBlock();
    }

    function getNewCumulativeSum(rewardPerBlock, totalPool, prevAP, blocksDelta) {
        return toBN(rewardPerBlock).mul(decimal).div(toBN(totalPool)).mul(toBN((blocksDelta))).add(toBN(prevAP));
    }

    function getUserreward(newAP, prevAP, userLiquidityAmount, prevReward) {
        return toBN(userLiquidityAmount).mul(toBN(newAP).sub(toBN(prevAP))).div(toBN(decimal)).add(toBN(prevReward));
    }
    
    before("Setup", async() => {
        testCoin = await TestCoin.new();
        staker = await Staking.new(testCoin.address, rewardPerBlock);
        
        snapshot = TimeTravaler.takeSnapshot();
        reverter.snapshot()    
    });

    afterEach("revert", revert);

    describe("updates", async() => {
        it.skip("test", async() => {
            await testCoin.approve(staker.address, 102);

            await staker.stake(100)
            await advanceBlocks(9);
            await staker.stake(1);
            await advanceBlocks(1);
            await staker.stake(1);

            let reward = (await staker.userStakes(OWNER)).reward;
            console.log(reward.toString());

        });

        it("should update cusum after one stake", async() => {
            await testCoin.approve(staker.address, AMOUNT+1);
            await staker.stake(AMOUNT);
            await advanceBlocks(blocks); // +9 blocks
            await staker.stake(1);

            let totalStaked = AMOUNT;
            let newSum = getNewCumulativeSum(rewardPerBlock,totalStaked,0,1+blocks);

            assert.equal(newSum.toString(), (await staker.sum()).toString()); 
        });

        it("should update cusum after two stakes", async() => {
            await testCoin.approve(staker.address, AMOUNT+1);
            await testCoin.transfer(ALICE, AMOUNT);
            await testCoin.approve(staker.address, AMOUNT, {from:ALICE});
            await staker.stake(AMOUNT, {from:ALICE});
            await staker.stake(AMOUNT);

            let totalStaked = AMOUNT;
            let newSum = getNewCumulativeSum(rewardPerBlock,totalStaked,0,1);
            assert.equal(newSum.toString(), (await staker.sum()).toString());
            

            await advanceBlocks(blocks);
            await staker.stake(1);

            totalStaked = AMOUNT*2;
            newSum = getNewCumulativeSum(rewardPerBlock,totalStaked,newSum,1+blocks);
            assert.equal(newSum.toString(), (await staker.sum()).toString());
        });

        it("should update reward after one stake", async() => {
            await testCoin.approve(staker.address, AMOUNT+1);
            await staker.stake(AMOUNT);
            await advanceBlocks(blocks); // +9 blocks
            await staker.stake(1);

            let totalStaked = AMOUNT;
            let newSum = getNewCumulativeSum(rewardPerBlock,totalStaked,0,1+blocks);
            let reward = getUserreward(newSum, 0, AMOUNT, 0).toString();
            assert.equal(reward.toString(), (await staker.userStakes(OWNER)).reward.toString());
            
        });

        it("should update reward after two stake", async() => {
            await testCoin.approve(staker.address, AMOUNT+1);
            await testCoin.transfer(ALICE, AMOUNT);
            await testCoin.approve(staker.address, AMOUNT, {from:ALICE});
            await staker.stake(AMOUNT, {from:ALICE});
            await staker.stake(AMOUNT);

            let lastSum = await staker.sum();
            
            await advanceBlocks(blocks);
            await staker.stake(1);

            totalStaked = AMOUNT*2;
            let newSum = getNewCumulativeSum(rewardPerBlock,totalStaked,lastSum,blocks*1+1);
            reward = getUserreward(newSum, lastSum, AMOUNT, 0).toString();
            assert.equal(reward.toString(), (await staker.userStakes(OWNER)).reward.toString());
            assert.equal(newSum.toString(), (await staker.sum()).toString());
            
        });
    });

    describe("stake", async() => {
        it("should stake 100", async() => {
            await testCoin.approve(staker.address, AMOUNT);
            await staker.stake(AMOUNT);
            let info = await staker.userStakes(OWNER);

            assert.equal(info.staked.toString(), AMOUNT);
            assert.equal(info.lastSum.toString(), 0);
            assert.equal(info.reward.toString(), 0);

            assert.equal((await staker.totalStaked()).toString(), AMOUNT);

            assert.equal((await testCoin.balanceOf(staker.address)).toString(), AMOUNT); 
        });

        it("should stake 100, wait 9 blocks, check reward", async() => {
            balance = await testCoin.balanceOf(OWNER);
            await testCoin.transfer(staker.address, AMOUNT*10);
            await testCoin.approve(staker.address, AMOUNT*2);
    
            await staker.stake(AMOUNT);

            assert.equal((await testCoin.balanceOf(OWNER)).toString(), balance.sub(toBN(AMOUNT*11)).toString());
            
            await advanceBlocks(blocks);

            await staker.stake(AMOUNT/2);
            
            let expectedReward = toBN(rewardPerBlock).mul(toBN(1+blocks));
            let actualInfo = await staker.userStakes(OWNER);
            assert.equal(actualInfo.reward.toString(), expectedReward.toString());
        });
        
        it("should stake 100 from OWNER, stake 100 from ALICE, wait 9 blocks, check reward", async() => {
            balance = await testCoin.balanceOf(OWNER);
            await testCoin.transfer(staker.address, AMOUNT*10);
            await testCoin.transfer(ALICE, AMOUNT*1+1);
            await testCoin.approve(staker.address, AMOUNT*2);
            await testCoin.approve(staker.address, AMOUNT*1+1, {from:ALICE});

            await staker.stake(AMOUNT);

            let info = await staker.userStakes(OWNER);
            assert.equal(info.staked.toString(), AMOUNT);
            assert.equal(info.lastSum.toString(), 0);
            assert.equal(info.reward.toString(), 0);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT);
           
            await advanceBlocks(blocks);

            await staker.stake(1);
            await staker.stake(AMOUNT, {from: ALICE});
            await staker.stake(1, {from: ALICE});

            info = await staker.userStakes(OWNER);
            assert.equal(info.reward.toString(), 100);
           

            info = await staker.userStakes(ALICE);
            assert.equal(info.staked.toString(), AMOUNT*1+1);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT*2+2);
           

            let expectedReward = toBN(rewardPerBlock).mul(toBN("10"));
            let actualInfo = await staker.userStakes(OWNER);
            assert.equal(actualInfo.reward.toString(), expectedReward.toString());
        });

        it("should stake 100 from four accounts, wait 9 blocks, check reward", async() => {
            let rewardPerBlock = 20;
            
            await testCoin.transfer(staker.address, AMOUNT*10);
            await testCoin.transfer(ALICE, AMOUNT*1+1);
            await testCoin.transfer(RON, AMOUNT*1+1);
            await testCoin.transfer(BOB, AMOUNT*1+1);

            await testCoin.approve(staker.address, AMOUNT*1+1);
            await testCoin.approve(staker.address, AMOUNT*1+1,{from:ALICE});
            await testCoin.approve(staker.address, AMOUNT*1+1,{from:RON});
            await testCoin.approve(staker.address, AMOUNT*1+1,{from:BOB});

            await staker.updateRewardPerBlock(rewardPerBlock);

            await staker.stake(AMOUNT);
            await staker.stake(AMOUNT, {from:ALICE});
            await staker.stake(AMOUNT, {from:RON});
            await staker.stake(AMOUNT, {from:BOB});

            await advanceBlocks(9);

            await staker.stake(1)
            await staker.stake(1, {from:ALICE});
            await staker.stake(1, {from:RON});
            await staker.stake(1, {from:BOB});

            let ownerReward = rewardPerBlock + rewardPerBlock/2 + rewardPerBlock/3 + rewardPerBlock/4 * 10;
            let aliceReward = rewardPerBlock/2 + rewardPerBlock/3 + rewardPerBlock/4 * 11;
            let ronReward = rewardPerBlock/3 + rewardPerBlock/4 * 12;
            let bobReward = rewardPerBlock/4 * 13;
            
            assert.equal(Math.floor(ownerReward), (await staker.userStakes(OWNER)).reward.toString());
            assert.equal(Math.floor(aliceReward), (await staker.userStakes(ALICE)).reward.toString());
            assert.equal(Math.floor(ronReward), (await staker.userStakes(RON)).reward.toString());
            assert.equal(Math.floor(bobReward)-1, (await staker.userStakes(BOB)).reward.toString());

        });

        it("should stake 66,33,1 from accounts, wait 9 blocks, check reward", async() => {
            let rewardPerBlock = 100;
            
            await testCoin.transfer(staker.address, AMOUNT*10);
            await testCoin.transfer(ALICE, AMOUNT*1+1);
            await testCoin.transfer(RON, AMOUNT*1+1);
            
            await testCoin.approve(staker.address, AMOUNT*1+1);
            await testCoin.approve(staker.address, AMOUNT*1+1,{from:ALICE});
            await testCoin.approve(staker.address, AMOUNT*1+1,{from:RON});
            
            await staker.updateRewardPerBlock(rewardPerBlock);

            await staker.stake(66);
            await staker.stake(33, {from:ALICE});
            await staker.stake(1, {from:RON});
            
            await advanceBlocks(9);

            await staker.stake(1)
            await staker.stake(1, {from:ALICE});
            await staker.stake(1, {from:RON});
            
            let onePercent = rewardPerBlock/100;

            let ownerReward = rewardPerBlock + Math.floor(onePercent*66)*11;
            let aliceReward = Math.floor(onePercent*33) * 12;
            let ronReward = onePercent * 11;

            assert.equal(Math.floor(ownerReward), (await staker.userStakes(OWNER)).reward.toString());
            assert.equal(Math.floor(aliceReward), (await staker.userStakes(ALICE)).reward.toString());
            assert.equal(Math.floor(ronReward), (await staker.userStakes(RON)).reward.toString());
        
        });

    });

    describe("withdraw", async() => {
        it("stake 100 withdraw 100", async() => {
            await testCoin.approve(staker.address, AMOUNT);
            await staker.stake(AMOUNT);
            
            let info = await staker.userStakes(OWNER);
            assert.equal(info.staked.toString(), AMOUNT);
            assert.equal(info.lastSum.toString(), 0);
            assert.equal(info.reward.toString(), 0);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT);
            assert.equal((await testCoin.balanceOf(staker.address)).toString(), AMOUNT);
            
            await staker.withdraw(AMOUNT);

            info = await staker.userStakes(OWNER);
            assert.equal(info.staked.toString(), 0);
            
            assert.equal(info.reward.toString(), 10);
            assert.equal((await staker.totalStaked()).toString(), 0);
            assert.equal((await testCoin.balanceOf(staker.address)).toString(), 0);
            assert.equal((await testCoin.balanceOf(OWNER)).toString(), balance.toString());        
        });

        it("stake 100, withdraw 50, check reward, withdraw all", async() => {
            await testCoin.approve(staker.address, AMOUNT);
            await staker.stake(AMOUNT);
            
            let info = await staker.userStakes(OWNER);
            assert.equal(info.staked.toString(), AMOUNT);
            assert.equal(info.lastSum.toString(), 0);
            assert.equal(info.reward.toString(), 0);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT);
            assert.equal((await testCoin.balanceOf(staker.address)).toString(), AMOUNT);
            
            await staker.withdraw(AMOUNT/2);

            info = await staker.userStakes(OWNER);
            assert.equal(info.staked.toString(), AMOUNT/2);
            
            assert.equal(info.reward.toString(), 10);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT/2);
            assert.equal((await testCoin.balanceOf(staker.address)).toString(), AMOUNT/2);
            
            advanceBlocks(blocks);

            let expectedReward = toBN(rewardPerBlock);
            let actualInfo = await staker.userStakes(OWNER);
            assert.equal(actualInfo.reward.toString(), expectedReward.toString());

            await staker.withdraw(AMOUNT/2);
        });

        it("should stake 100 from OWNER, 100 from ALICE, withdraw 50, check reward", async() => {
            balance = await testCoin.balanceOf(OWNER);
            await testCoin.transfer(staker.address, AMOUNT*10);
            await testCoin.transfer(ALICE, AMOUNT);
            await testCoin.approve(staker.address, AMOUNT*2);
            await testCoin.approve(staker.address, AMOUNT, {from:ALICE});
            
            await staker.stake(AMOUNT);

            let info = await staker.userStakes(OWNER);
            assert.equal(info.staked.toString(), AMOUNT);
            assert.equal(info.lastSum.toString(), 0);
            assert.equal(info.reward.toString(), 0);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT);
            
            await staker.stake(AMOUNT, {from:ALICE});

            info = await staker.userStakes(ALICE);
            assert.equal(info.staked.toString(), AMOUNT);
            
            assert.equal(info.reward.toString(), 0);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT*2);
            
            advanceBlocks(blocks);

            await staker.withdraw(AMOUNT/2);

            info = await staker.userStakes(OWNER);
            assert.equal(info.staked.toString(), AMOUNT/2);
            
            assert.equal(info.reward.toString(), 30);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT*1+AMOUNT/2);

            advanceBlocks(blocks);

            await staker.withdraw(AMOUNT/2);

            
            actualInfo = await staker.userStakes(OWNER);
            assert.equal(actualInfo.reward.toString(), 63);
        });

        it("should revert when try draw", async() => {
            await truffleAssert.reverts(staker.withdraw(10), "Amount more then stake");
        });
    });

    describe("drawReward", async() => {
        it("stake 100, drawReward", async() => {
            await testCoin.approve(staker.address, AMOUNT+1);
            await staker.stake(AMOUNT);

            let info = await staker.userStakes(OWNER);
            assert.equal(info.staked.toString(), AMOUNT);
            assert.equal(info.lastSum.toString(), 0);
            assert.equal(info.reward.toString(), 0);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT);
            assert.equal((await testCoin.balanceOf(staker.address)).toString(), AMOUNT);
            
            await advanceBlocks(blocks);

            await staker.stake(1);

            info = await staker.userStakes(OWNER);
            assert.equal(info.staked.toString(), AMOUNT*1+1);
            
            assert.equal(info.reward.toString(), 100);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT*1+1);
            assert.equal((await testCoin.balanceOf(staker.address)).toString(), AMOUNT*1+1);
            
            let totalStaked = AMOUNT;
            let newSum = getNewCumulativeSum(rewardPerBlock,totalStaked,0,blocks*1+1);
            reward = getUserreward(newSum, 0, AMOUNT, 0).toString();

            balance = await testCoin.balanceOf(OWNER);
            await staker.drawReward(toBN(reward));
            assert.equal(balance.add(toBN(reward)).toString(), (await testCoin.balanceOf(OWNER)).toString());

        });

        it("should stake 100 twise, draw reward", async() => {
            await testCoin.approve(staker.address, AMOUNT*2+1);
            await staker.stake(AMOUNT);

            let info = await staker.userStakes(OWNER);
            assert.equal(info.staked.toString(), AMOUNT);
            assert.equal(info.lastSum.toString(), 0);
            assert.equal(info.reward.toString(), 0);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT);
            assert.equal((await testCoin.balanceOf(staker.address)).toString(), AMOUNT);
            
            await staker.stake(AMOUNT);

            await advanceBlocks(blocks);

            
            await staker.stake(1);
            

            totalStaked = AMOUNT*2;
            let newSum = getNewCumulativeSum(rewardPerBlock,totalStaked,0,blocks*1+2);
            reward = getUserreward(newSum, 0, AMOUNT*2, 0).toString();
            

            info = await staker.userStakes(OWNER);
            assert.equal(info.staked.toString(), AMOUNT*2+1);
            
            assert.equal(info.reward.toString(), reward);
            assert.equal((await staker.totalStaked()).toString(), AMOUNT*2+1);
            assert.equal((await testCoin.balanceOf(staker.address)).toString(), AMOUNT*2+1);
            
            balance = await testCoin.balanceOf(OWNER);
            await staker.drawReward(reward);
            assert.equal(balance.add(toBN(reward)).toString(), (await testCoin.balanceOf(OWNER)).toString());
        });

        it("should test reward", async() => {
            let balance = await testCoin.balanceOf(OWNER);
            await testCoin.approve(staker.address, AMOUNT);
            await testCoin.transfer(ALICE, AMOUNT);
            await testCoin.approve(staker.address, AMOUNT, {from:ALICE});
            
            await staker.stake(AMOUNT);
            await staker.stake(AMOUNT, {from:ALICE});

            await advanceBlocks(blocks);

            await staker.drawReward(AMOUNT/2);
            
            await staker.drawReward(AMOUNT/2,{from:ALICE});

            assert.equal(balance.sub(toBN(AMOUNT)).sub(toBN(AMOUNT).div(toBN("2"))).toString(), (await testCoin.balanceOf(OWNER)).toString());
            assert.equal(AMOUNT/2, (await testCoin.balanceOf(ALICE)));
        });

        it("should revert when try draw reward", async() => {
            await truffleAssert.reverts(staker.drawReward(10), "Amount more then reward");
        });
    });

    describe("updateRewardPerBlock", async() => {
        it("should set reward 10, wait 10 blocks, set reward 20, wait 10 blocks", async() => {        
            await testCoin.approve(staker.address, 101);
            await staker.updateRewardPerBlock(10);
            assert.equal(10, (await staker.rewardPerBlock()).toString());

            await staker.stake("100", {from: OWNER});
            await advanceBlocks(9);

            await staker.updateRewardPerBlock(20);
            assert.equal(20, (await staker.rewardPerBlock()).toString());
            
            await advanceBlocks(9);
            await staker.stake(1 , {from:OWNER});

            let reward = (await staker.userStakes(OWNER)).reward;
            
            assert.equal(300, reward.toString());
        });

        it("should stake, change reward, stake from another account", async() => {
            await testCoin.approve(staker.address, 101);
            await testCoin.transfer(ALICE, 101);
            await testCoin.approve(staker.address,101, {from:ALICE});
    

            await staker.updateRewardPerBlock(10);
            assert.equal(10, (await staker.rewardPerBlock()).toString());

            await staker.stake("100", {from: OWNER});
            await advanceBlocks(9);

            await staker.updateRewardPerBlock(20);
            assert.equal(20, (await staker.rewardPerBlock()).toString());
            
            await staker.stake("100", {from:ALICE});

            await advanceBlocks(8);
            await staker.stake(1 , {from:OWNER});
            await staker.stake(1, {from:ALICE});

            let reward = (await staker.userStakes(OWNER)).reward;
            assert.equal(210, reward.toString());

            reward = (await staker.userStakes(ALICE)).reward;
            assert.equal(100-1, reward.toString());
        });

        it("should change reward trice", async() => {
            await testCoin.approve(staker.address, 101);
            let reward1 = 10**10;
            let reward2 = 10**5;
            let reward3 = 10;
            await staker.updateRewardPerBlock(reward1);
            assert.equal(reward1, (await staker.rewardPerBlock()).toString());

            await staker.stake(100, {from:OWNER});
            await advanceBlocks(9);
            
            await staker.updateRewardPerBlock(reward2);
            assert.equal(reward2, (await staker.rewardPerBlock()).toString());

            await advanceBlocks(9);
            
            await staker.updateRewardPerBlock(reward3);
            assert.equal(reward3, (await staker.rewardPerBlock()).toString());

            await advanceBlocks(9);
            await staker.stake(1, {from:OWNER});

            let expected = reward1*10 + reward2*10 + reward3*10;

            assert.equal(expected, (await staker.userStakes(OWNER)).reward.toString());
        });

        it("should change reward and stake trice", async() => {
            await testCoin.approve(staker.address, 101);
            await testCoin.transfer(ALICE, 101);
            await testCoin.approve(staker.address, 101, {from:ALICE});
            await testCoin.transfer(RON, 101);
            await testCoin.approve(staker.address, 101, {from: RON});
            let reward1 = 10**11;
            let reward2 = 10**5;
            let reward3 = 5;
            await staker.updateRewardPerBlock(reward1);
            assert.equal(reward1, (await staker.rewardPerBlock()).toString());

            await staker.stake(100, {from:OWNER});
            await advanceBlocks(9);
            
            await staker.updateRewardPerBlock(reward2);
            assert.equal(reward2, (await staker.rewardPerBlock()).toString());
            await staker.stake(100, {from:ALICE});

            await advanceBlocks(9);
            
            await staker.updateRewardPerBlock(reward3);
            assert.equal(reward3, (await staker.rewardPerBlock()).toString());
            await staker.stake(100, {from:RON});

            await advanceBlocks(9);
            await staker.stake(1, {from:OWNER});
            await staker.stake(1, {from:ALICE});
            await staker.stake(1, {from:RON});

            let expected1 = reward1*10 + reward2*2 + (reward2*8)/2 + reward3/2 + (reward3*10)/3;
            let expected2 = (reward2*10)/2 + (reward3*11)/3 + reward3/2;
            let expected3 = (reward3*10)/3 + reward3/2;

            assert.equal(Math.floor(expected1), (await staker.userStakes(OWNER)).reward.toString());
            assert.equal(Math.floor(expected2), (await staker.userStakes(ALICE)).reward.toString());
            assert.equal(Math.floor(expected3), (await staker.userStakes(RON)).reward.toString());
        });
      });


  describe("stake from several accounts and check reward", async() => {
    it("should stake from five account, same amount", async() => {
        let reward = 100;
        
        await staker.updateRewardPerBlock(0);
        
        await testCoin.approve(staker.address, 101);
        await testCoin.transfer(RON, 101);
        await testCoin.approve(staker.address, 101, {from: RON});
        await testCoin.transfer(USER4, 101);
        await testCoin.approve(staker.address, 101, {from: USER4});
        await testCoin.transfer(ALICE, 101);
        await testCoin.approve(staker.address,101,{from:ALICE});
        await testCoin.transfer(BOB, 101);
        await testCoin.approve(staker.address,101,{from:BOB});

        await staker.stake(100);
        await staker.stake(100,{from:ALICE});
        await staker.stake(100,{from:BOB});
        await staker.stake(100,{from:RON});
        await staker.stake(100,{from:USER4});

        await staker.updateRewardPerBlock(reward);

        await advanceBlocks(9);

        await staker.stake(1);
        await staker.stake(1,{from:ALICE});
        await staker.stake(1,{from:BOB});
        await staker.stake(1,{from:RON});
        await staker.stake(1,{from:USER4});

        let expected = (reward/5)*10;

        assert.equal(expected, (await staker.userStakes(OWNER)).reward.toString());
        assert.equal(expected+20-1, (await staker.userStakes(ALICE)).reward.toString());
        assert.equal(expected+20*2-1, (await staker.userStakes(BOB)).reward.toString());
        assert.equal(expected+20*3-1, (await staker.userStakes(RON)).reward.toString());
        assert.equal(expected+20*4-1, (await staker.userStakes(USER4)).reward.toString());
    });

    it("should stake from five accounts, amounts 40-30-15-10-5", async() => {
      let reward = 100;
      let percent = reward/100;

      await staker.updateRewardPerBlock(0);
      
      await testCoin.approve(staker.address, 41);
      await testCoin.transfer(RON, 11);
      await testCoin.approve(staker.address, 11, {from: RON});
      await testCoin.transfer(USER4, 6);
      await testCoin.approve(staker.address, 6, {from: USER4});
      await testCoin.transfer(ALICE, 31);
      await testCoin.approve(staker.address,31,{from:ALICE});
      await testCoin.transfer(BOB, 16);
      await testCoin.approve(staker.address,16,{from:BOB});

      await staker.stake(40);
      await staker.stake(30,{from:ALICE});
      await staker.stake(15,{from:BOB});
      await staker.stake(10,{from:RON});
      await staker.stake(5,{from:USER4});

      await staker.updateRewardPerBlock(reward);

      await staker.stake(1);
      await staker.stake(1,{from:ALICE});
      await staker.stake(1,{from:BOB});
      await staker.stake(1,{from:RON});
      await staker.stake(1,{from:USER4});

      let expected1 = percent*40;
      let expected2 = percent*30*2;
      let expected3 = percent*15*3;
      let expected4 = percent*10*4;
      let expected5 = percent*5*5;

      assert.equal(expected1, (await staker.userStakes(OWNER)).reward.toString());
      assert.equal(expected2-1, (await staker.userStakes(ALICE)).reward.toString());
      assert.equal(expected3-1, (await staker.userStakes(BOB)).reward.toString());
      assert.equal(expected4-1, (await staker.userStakes(RON)).reward.toString());
      assert.equal(expected5-1, (await staker.userStakes(USER4)).reward.toString());
    });
      
  });

    
});
